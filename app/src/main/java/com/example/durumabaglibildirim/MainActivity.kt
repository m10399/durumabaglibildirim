package com.example.durumabaglibildirim

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.durumabaglibildirim.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding.buttonBildir.setOnClickListener {
            try {
                val builder : NotificationCompat.Builder
                val bildirimYoneticisi = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                val intent = Intent(this,MainActivity::class.java)
                val gidilecekIntent = if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.S){
                    PendingIntent.getActivity(this, 1, intent,  PendingIntent.FLAG_IMMUTABLE)
                }
                else{
                    PendingIntent.getActivity(this,1,intent,PendingIntent.FLAG_UPDATE_CURRENT)
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

                    val kanalId = "kanalId"
                    val kanalAd = "kanalAd"
                    val kanalTanitim = "KanalTanıtım"
                    val kanalOnceligi = NotificationManager.IMPORTANCE_HIGH

                    var kanal : NotificationChannel? = bildirimYoneticisi.getNotificationChannel(kanalId)
                    if (kanal == null){
                        kanal = NotificationChannel(kanalId,kanalAd,kanalOnceligi)
                        kanal.description = kanalTanitim
                        bildirimYoneticisi.createNotificationChannel(kanal)
                    }
                    builder = NotificationCompat.Builder(this,kanalId)
                    builder.setContentTitle("Başlık")
                        .setContentText("İçerik")
                        .setSmallIcon(R.drawable.ic_audiotrack)
                        .setContentIntent(gidilecekIntent)
                        .setAutoCancel(true)
                }
                else{
                    builder = NotificationCompat.Builder(this)
                    builder.setContentTitle("Başlık")
                        .setContentText("İçerik")
                        .setSmallIcon(R.drawable.ic_audiotrack)
                        .setContentIntent(gidilecekIntent)
                        .setAutoCancel(true)
                        .priority= Notification.PRIORITY_HIGH
                }

                bildirimYoneticisi.notify(1,builder.build())
            }catch (e:Exception){
                e.printStackTrace()
            }

        }
    }
}